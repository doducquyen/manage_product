<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Cate;
use App\Product;
class ProductController extends Controller {

	public function getAdd() {
		$cate = Cate::select('id','name','parent_id')->get()->toArray();
		return view('admin.product.add', compact('cate'));
	}

	public function postAdd(ProductRequest $request) {
		$file_name = $request->file('fImages')->getClientOriginalName();
		$product = new Product();
		$product->name = $request->txtName;
		$product->alias = changeTitle($request->txtName);
		$product->price = $request->txtPrice;
		$product->intro = $request->txtIntro;
		$product->content = $request->txtContent;
		$product->image = $file_name;
		$product->keywords = $request->txtKeywords;
		$product->description = $request->txtDescription;
		$product->user_id = 1;
		$product->cate_id = $request->sltCateParent;
		$request->file('fImages')->move('resources/upload/',$file_name);
		$product->save();
	}
}
