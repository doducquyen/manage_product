<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'sltCateParent' => 'required',
			'txtName' => 'required|unique:products,name',
			'fImages' => 'required|image'
		];
	}

	public function messages() {
		return [
			'sltCateParent.required' => 'Please choose category',
			'txtName.required' => 'Please enter name',
			'txtName.unique' => 'Duplicate name',
			'fImages.required' => 'Please insert image',
			'fImages.image' => 'This is not image'
		];
	}
}
